jQuery(document).ready(function($) {
  $('.accordion-content').hide();
  $('.accordion-toggle h3').prepend('<i class="fa fa-plus"></i>');
  $('.accordion-toggle').each(function(index, el) {
    $(this).click(function(event) {
      event.preventDefault();
      $(this).toggleClass('open');
      changeIcon($(this));
      $(this).siblings('.accordion-content').slideToggle(500);
    });
  });

  function changeIcon(lol) {
    if (lol.find('.fa').hasClass('fa-plus')) {
      lol.find('.fa').removeClass('fa-plus').addClass('fa-minus');
    } else {
      lol.find('.fa').removeClass('fa-minus').addClass('fa-plus');
    }

  }
});