'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
/*
* Require the path module
*/
const path = require('path');

const fractal = require('@frctl/fractal').create();
fractal.components.set('path', path.join(__dirname, 'components'));
fractal.docs.set('path', path.join(__dirname, 'docs'));
fractal.web.set('static.path', path.join(__dirname, 'public'));

fractal.set('project.title', 'SUNY Oswego style guide');
fractal.set('project.version', 'v1.0');
fractal.set('project.author', 'Joe Fitzsimmons');

const mandelbrot = require('@frctl/mandelbrot'); // require the Mandelbrot theme module

// create a new instance with custom config options
const myCustomisedTheme = mandelbrot({
    "skin": "white",
    "nav": ["docs", "components"],
    "styles": [
        "https://cloud.typography.com/6053872/804304/css/fonts.css",
        "default"
    ]
    // any other theme configuration values here
});

fractal.web.theme(myCustomisedTheme); // tell Fractal to use the configured theme by default

const logger = fractal.cli.console; // keep a reference to the fractal CLI console utility

/*
 * Start the Fractal server
 *
 * In this example we are passing the option 'sync: true' which means that it will
 * use BrowserSync to watch for changes to the filesystem and refresh the browser automatically.
 * Obviously this is completely optional!
 *
 * This task will also log any errors to the console.
 */

gulp.task('fractal:start', function(){
    const server = fractal.web.server({
        sync: true
    });
    server.on('error', err => logger.error(err.message));
    return server.start().then(() => {
        logger.success(`Fractal server is now running at ${server.url}`);
    });
});



gulp.task('sass', function () {
  return gulp.src('./components/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./components'));
});

gulp.task('sass:watch', function () {
  gulp.watch('/components/**/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'fractal:start', 'sass:watch']);

/*
 * Run a static export of the project web UI.
 *
 * This task will report on progress using the 'progress' event emitted by the
 * builder instance, and log any errors to the terminal.
 *
 * The build destination will be the directory specified in the 'builder.dest'
 * configuration option set above.
 */

gulp.task('fractal:build', function(){
    const builder = fractal.web.builder();
    builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
    builder.on('error', err => logger.error(err.message));
    return builder.build().then(() => {
        logger.success('Fractal build completed!');
    });
});