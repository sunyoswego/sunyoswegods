---
title: Principles
---

1. Begin with the user in mind
2. Who uses your site?
3. What questions account for the most number of phone calls/requests to your office?
4. Is there a way to display that information on your website to help mitigate requests?

## Content first

No matter what you are creating, you need a solid message. What are you trying to communicate to your users? When working with digital services, knowing *what* you are saying and to *who* is a vital pre-requisite to creating an awesome web site. Without appropriate goals and content, we can only decorate a web site for you. With the proper goals and content, we can design an experience that meet's your goals.
