'use strict';

/*
* Require the path module
*/
const path = require('path');

/*
 * Require the Fractal module
 */
const fractal = module.exports = require('@frctl/fractal').create();

/*
 * Give your project a title.
 */
fractal.set('project.title', 'Sunyoswegods');

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path', path.join(__dirname, 'components'));

/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', path.join(__dirname, 'docs'));

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path', path.join(__dirname, 'public'));

fractal.set('project.title', 'SUNY Oswego style guide');
fractal.set('project.version', 'v1.0');
fractal.set('project.author', 'Joe Fitzsimmons');

const mandelbrot = require('@frctl/mandelbrot'); // require the Mandelbrot theme module

// create a new instance with custom config options
const myCustomisedTheme = mandelbrot({
    "skin": "white",
    "nav": ["docs", "components"],
    "styles": [
        "https://cloud.typography.com/6053872/804304/css/fonts.css",
        "default",
        "https://www.oswego.edu/sites/all/themes/oswego/assets/css/oswego.style.css",
    ]
    // any other theme configuration values here
});

fractal.web.theme(myCustomisedTheme); // tell Fractal to use the configured theme by default